var gulp = require('gulp');
var iconfont = require('gulp-iconfont');
var iconfontCSS = require('gulp-iconfont-css');
var jade = require('gulp-jade');

var svg2font = './svg2font/';
var cssPathForPreview = './svg2font/'
var fontName = 'PlatformIcons';
var className = 'pi';
 
gulp.task('generate-icon-font', function() {
  gulp.src([svg2font + 'src/svg/*.svg'])
    .pipe(iconfontCSS({
      fontName: fontName,
      cssClass: className,
      targetPath: '../../src/css/icons.css',
      fontPath: '../../build/font/'
    }))
    .pipe(iconfont({
      fontName: fontName,
      // Remove woff2 if you get an ext error on compile
      formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'],
      normalize: true,
      fontHeight: 10001,
      centerHorizontally: true
    }))
    .on('glyphs', function(glyphs, options) {
      doHtml(glyphs);
  })
    .pipe(gulp.dest(svg2font + 'build/font/'))
});

function doHtml( glyphs ){
    var data = [];
    glyphs.map((e,i) => {
      data.push(e.name);
    });

    gulp.src(svg2font + 'src/jade/index.jade')
        .pipe(jade({
          locals: {
            icons: data,
            className: className,
            fontName: fontName
          }
        }))
        .pipe(gulp.dest(svg2font + 'build/'))
}